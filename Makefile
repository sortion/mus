target=mus
TEXMFHOME=$(shell kpsewhich -var-value=TEXMFHOME)
LATEXHOME=$(TEXMFHOME)/tex/latex/

all: docstrip doc

docstrip: 
	tex $(target).dtx

doc:
	lualatex -shell-escape -file-line-error -interaction=nonstopmode $(target).dtx

error_doc:
	lualatex -shell-escape -file-line-error $(target).dtx

install: docstrip
	mkdir -p $(LATEXHOME)
	cp *.cls $(LATEXHOME)
	cp *.sty $(LATEXHOME)
