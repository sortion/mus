# mus - A rodent's LaTeX style

## Setup

1. Clone the repository <https://framagit.org/sortion/mus>
2. Change to the `mus` directory
3. Run `make install` to extract the `.sty` file and put it in the `TEXTMFHOME` latex custom installation folder.

## Usage

`\usepackage{mus}`

The [mus.pdf](./mus.pdf) file should hold some basic documentation.


  \label{prf:4-color-thm}

